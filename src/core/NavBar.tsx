import React from 'react';
import { NavLink, useHistory } from 'react-router-dom';
import { signOut } from './auth/authentication.service';
import { IfLogged } from './auth/IfLogged';

export const NavBar: React.FC = () => {
  const history = useHistory();

  function logoutHandler() {
    signOut();
    history.push('/login/signin');
  }

  return (
    <nav className="navbar navbar-expand navbar-light bg-light">
      <div className="navbar-brand">
        <NavLink to="/login">React Auth</NavLink>
      </div>

      <div className="collapse navbar-collapse" id="navbarNav">
        <ul className="navbar-nav">
          <li className="nav-item">
            <NavLink
              activeClassName="active"
              className="nav-link"
              to="/home">Home</NavLink>
          </li>

          <IfLogged role="admin">
            <li className="nav-item">
              <NavLink
                activeClassName="active"
                className="nav-link"
                to="/admin">Admin</NavLink>
            </li>
          </IfLogged>

          <li className="nav-item">
            <NavLink
              activeClassName="active"
              exact={ true }
              className="nav-link"
              to="/settings">Settings</NavLink>
          </li>
          <li>
            <NavLink
              activeClassName="active"
              exact={ true }
              className="nav-link"
              to="/users">users</NavLink>
          </li>

          <IfLogged role="admin">
            <li onClick={logoutHandler} className="nav-link">
            Quit
            </li>
          </IfLogged>
        </ul>
      </div>
    </nav>
  )
};

