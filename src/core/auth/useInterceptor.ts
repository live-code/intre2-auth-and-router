import { useEffect, useState } from 'react';
import axios, { AxiosRequestConfig } from 'axios';
import { getToken } from './authentication.service';

export function useInterceptor() {
  const [error,setError] = useState<boolean>()
  const [request,setRequest] = useState<AxiosRequestConfig>()

  useEffect(() => {

    axios.interceptors.request.use(function (config) {
      const cfg = {
        ...config,
        headers: {
          ...config.headers,
          'Authorization': 'bearer ' + getToken()
        }
      }
      setError(false)
      setRequest(cfg);
      return cfg;
    }, function (error) {
      // Do something with request error
      return Promise.reject(error);
    });

// Add a response interceptor
    axios.interceptors.response.use(function (response) {
      // Any status code that lie within the range of 2xx cause this function to trigger
      // Do something with response data
      console.log('response', response)
      return response;
    }, function (error) {
      // Any status codes that falls outside the range of 2xx cause this function to trigger
      // Do something with response error
      console.log('error', error)
      setError(true)
      return Promise.reject(error);
    });

  }, [])
  return {
    error,
    request
  }
}
