import axios from 'axios';
import { useState } from 'react';

export interface Credentials {
  username: string;
  password: string;
}

export interface Auth {
  token: string;
  role: string;
  displayName: string;
}

export function useAuthentication () {
  const [auth, setAuth] = useState();
  const [error, setError] = useState<boolean>();

  function signIn(credentals: Credentials): Promise<Auth> {
    setError(false)
    return axios.get<Auth>('http://localhost:3001/login')
      .then(res => {
        localStorage.setItem('auth', JSON.stringify(res.data))
        setAuth(auth)
        return res.data;
      })
      .catch((err) => {
        setError(true)
        return err;
      })
  }


  function signOut() {
    localStorage.removeItem('auth')
  }

  function isLogged(): boolean {
    return !!localStorage.getItem('auth')
  }

  function getRole(): string {
    const auth: Auth = JSON.parse(localStorage.getItem('auth') || '') as Auth;
    return auth.role;
  }

  function getToken(): string {
    const auth: Auth = JSON.parse(localStorage.getItem('auth') || '') as Auth;
    return auth.token;
  }

  return {
    auth,
    signIn,
    error,
    token: getToken()
  }

}


