import React  from 'react';
import { getRole, isLogged } from './authentication.service';

interface IfLoggedProps {
  role: string;
}
export const IfLogged: React.FC<IfLoggedProps> = (props) => {
  return (
    <div>{isLogged() && (props.role === getRole()) ?
      props.children : null 
    }</div>
  )
};
