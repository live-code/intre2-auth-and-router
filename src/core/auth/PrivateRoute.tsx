import React  from 'react';
import { Redirect, Route, RouteProps } from 'react-router-dom';
import { isLogged } from './authentication.service';

interface PrivateRouteProps {
  color: string;
}
export const PrivateRoute: React.VFC<RouteProps & PrivateRouteProps> = ({ color, ...params }) => {
  return <Route {...params}>
    {
      isLogged() ?
        params.children :
        <Redirect to={{ pathname: '/login/signin'}} />
    }
  </Route>
};
