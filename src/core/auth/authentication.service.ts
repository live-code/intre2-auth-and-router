import axios from "axios";

export interface Credentials {
  username: string;
  password: string;
}

export interface Auth {
  token: string;
  role: string;
  displayName: string;
}


export function signIn(credentals: Credentials): Promise<Auth> {
  return axios.get<Auth>('http://localhost:3001/login')
    .then(res => {
      localStorage.setItem('auth', JSON.stringify(res.data))
      return res.data;
    })
}


export function signOut() {
  localStorage.removeItem('auth')

}

export function isLogged(): boolean {
  return !!localStorage.getItem('auth')
}

export function getRole(): string {
  const auth: Auth = JSON.parse(localStorage.getItem('auth') || '') as Auth;
  return auth.role;
}

export function getToken(): string {
  const auth: Auth = JSON.parse(localStorage.getItem('auth') || '') as Auth;
  return auth.token;
}

