import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { NavLink, RouteComponentProps, useParams } from 'react-router-dom';
import { User } from '../model/user';

interface Params {
  id: string
}
export const PageUsersDetails: React.VFC<RouteComponentProps<Params>> = () => {
  const [user, setUser] = useState<User>();
  const [error, setError] = useState<boolean>();
  const params = useParams<Params>();

  useEffect(() => {
    getUser(params.id)
  }, [params.id]);

  async function getUser(id: string) {
    /*
    axios.get<User>(`http://localhost:3001/users/${id}`)
      .then(res => setUser(res.data))
      .catch(err => setError(true))
    */

    try {
      const res = await axios.get<User>(`http://localhost:3001/users/${id}`)
      setError(false)
      setUser(res.data)
    } catch(err) {
      setError(true)
    }

    /*
    Promise.all([
      axios.get<User[]>(`http://localhost:3001/users`),
      axios.get<User>(`http://localhost:3001/users/3`)
    ])
      .then(([users, user]) => {
        setUser(user.data)
      })
   */
  }


  return (
    <div>
      { error && <div className="alert alert-danger">error</div>}
      {user?.name}

      <hr/>
      <NavLink to="/users/4">
        <button>go to next</button>
      </NavLink>
    </div>
  )
}
