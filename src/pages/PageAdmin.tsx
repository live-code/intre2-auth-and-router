import React, { useEffect } from 'react';
import axios from 'axios';
import { User } from '../model/user';
import { getToken } from '../core/auth/authentication.service';

const PageAdmin: React.VFC = () => {
  useEffect(() => {
    axios.get<User>(`http://localhost:3001/users`)
  }, [])
  return <div>PageAdmin</div>
}
export default PageAdmin;




// http.service.ts
// usage: get<User>('http:/...')

function get<T>(url: string): Promise<T> {
  return axios.get<T>(url, {
    headers: {
      AUthorization: 'bearer ' + getToken()
    }
  })
    .then(res => res.data)
}
