import React from 'react';
import { Link, Route, RouteComponentProps, Switch } from 'react-router-dom';
import { PageSignIn } from './components/PageSignIn';

export const PageLogin: React.VFC<RouteComponentProps> = ({  match  }) => {
  return (
    <div>
      <Link to={`${match.path}/signin`}>SignIn</Link> -
      <Link to={`${match.path}/lostpass`}>Lost pass</Link>  -
      <Link to={`${match.path}/registration`}>Registration</Link>
      <hr/>

      {/*<Route path="*" component={BreadCrumbs} />*/}
      <Route path={`${match.path}/:section`} component={BreadCrumbs} />

      <Switch>
        <Route path="/login/signin" component={PageSignIn} />

        <Route path="/login/registration">
          <h1>Registration</h1>
          <input type="text"/>
        </Route>

        <Route path="/login/lostpass">
          <h1>lostpass</h1>
          <input type="text"/>
        </Route>

        <Route exact path={match.path}>
          Seleziona una voce
        </Route>
      </Switch>
    </div>
  )
}

const BreadCrumbs = (props: RouteComponentProps<{section: string}>) => {
  return <div>
    {props.location.pathname}
    <br/>
    login / {props.match.params.section}
  </div>
}
