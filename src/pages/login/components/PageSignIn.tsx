import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useAuthentication } from '../../../core/auth/useAuthentication';

export const PageSignIn: React.FC = () => {
  const history = useHistory();

  const { error, signIn, token} = useAuthentication();

  function signInHandler() {
    signIn({ username: 'abc', password: '123' })
      .then(res => {
        history.push('/admin')
      })
  }

  return (
    <div>
      <h1>Sign IN FORM</h1>
      { error && <div className="alert alert-danger">errore</div>}
      <input type="text"/>
      <input type="text"/>

      <button onClick={signInHandler}>SIGNIN</button>
    </div>
  )
};
