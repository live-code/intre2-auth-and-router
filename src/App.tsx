import React, { lazy, Suspense, useEffect, useState } from 'react';
import './App.css';
import { PageHome } from './pages/PageHome';
import { PageLogin } from './pages/login/PageLogin';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import { NavBar } from './core/NavBar';
import { PageUsers } from './pages/PageUsers';
import { PageUsersDetails } from './pages/PageUsersDetails';
import { PrivateRoute } from './core/auth/PrivateRoute';
import axios from 'axios';
import { getToken } from './core/auth/authentication.service';
import { useInterceptor } from './core/auth/useInterceptor';

const PageSettings = lazy(() => import('./pages/PageSettings'));
const PageAdmin = lazy(() => import('./pages/PageAdmin'));

function App() {
  const { error, request } = useInterceptor();

  return (
    <div>
      {error && <div className="alert alert-danger">error</div>}
      <BrowserRouter>
        <Suspense fallback={<div>loading</div>}>

          <Route path="*" component={NavBar} />

          <Switch>
            <Route path="/login" component={PageLogin} />

            <Route path="/home">
              <PageHome />
            </Route>

            <PrivateRoute path="/admin" color="red" >
              <PageAdmin />
            </PrivateRoute>

            <Route path="/settings">
              <PageSettings />
            </Route>

            <Route path="/users" exact>
              <PageUsers />
            </Route>

            <Route path="/users/:id" component={PageUsersDetails} />

            <Route path="*">
              <Redirect to={{ pathname: '/login'}} />
            </Route>
          </Switch>
        </Suspense>
      </BrowserRouter>
    </div>

  );
}

export default App;
